package com.main;

import com.NeuralNetwork.NeuralMap;
import com.NeuralNetwork.Node;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Vector;

public class Main {

    public static void main(String[] args) {
	// write your code here

        NeuralMap map = new NeuralMap(4, 2, 3, 3, 2);
        map.createNodes();
        //map.addBias(2, .35);
        //map.addBias(3, .35);
        //map.addBias(4, .6);
        //map.addBias(5, .6);
//
        //map.changeWeight(0, 2, .15);
        //map.changeWeight(0, 3, .25);
        //map.changeWeight(1, 2, .2);
        //map.changeWeight(1, 3, .3);
        //map.changeWeight(2, 4, .4);
        //map.changeWeight(2, 5, .5);
        //map.changeWeight(3, 4, .45);
        //map.changeWeight(3, 5, .55);

        //map.setInputValues("1", "2", "3", "4", "5");
        //map.setOutputValues("Left", "Down", "Up", "Right");

        //NeuralMap map = NeuralMap.readData(new File("tmp.csv"));
        for(String str : map.getNodeDetailStrings()){
            System.out.println(str);
        }

        for(int i = 0; i < 10000; i++) {
            for (double value : map.forward(new double[]{0.05, .1}, true)) {
                System.out.println("" + value);
            }
            List<double[]> array = new ArrayList<>();
            array.add(new double[]{.01, .99});
            map.backpropagation(array);
        }
        try{
            map.saveData(new File("tmp.csv"));
        }catch  (Exception e){

        }
    }
}
