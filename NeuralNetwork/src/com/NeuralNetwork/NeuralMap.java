package com.NeuralNetwork;

import com.sun.deploy.util.OrderedHashSet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by rpai on 6/27/2018.
 */
public class NeuralMap {

    private int layercount = 0;
    private int[] layerNodes;
    Vector<Set<Node>> vectorSets = null;
    Map<Integer, Node> nodeDictionary = null;
    private Map<Integer, Double> biasDictionary;
    private Map<Integer, Double> learningRateDictionary;
    private double defaultLearningRate;
    private double defaultBias;
    private CalculationStorage storage;

    protected NeuralMap(){
        Node.resetNodeNumber();
        this.storage = new CalculationStorage();
        this.biasDictionary = new Hashtable<>();
        this.learningRateDictionary = new Hashtable<>();
        this.defaultLearningRate = 0.5;
        this.defaultBias = 0;
    }

    /**
     * constructor
     * @param layerCount number of hidden layers
     * @param layerNodeCount hidden layer node numbers
     */
    public NeuralMap(int layerCount, int... layerNodeCount ){
        this();

        this.layercount = layerCount;
        this.layerNodes = new int[layercount];
        for(int i = 0; i < layerCount; i++){
            if(layerNodeCount[i] > 0){
                layerNodes[i] = layerNodeCount[i];
            } else {
                throw new IllegalArgumentException("Invalid number for number of nodes in hidden layer");
            }
        }
    }

    public NeuralMap(String[] args){
        this();

        this.layercount = Integer.parseInt(args[0]);
        this.layerNodes = new int[layercount];
        for(int i = 1; i < args.length; i++)
        {
            int layer = Integer.parseInt(args[i]);
            if(layer > 0){
                layerNodes[i - 1] = layer;
            } else {
                throw new IllegalArgumentException("Invalid number for number of nodes in hidden layer");
            }
        }
    }

    /**
     * Get all nodes
     * @return
     */
    public Collection<String> getNodeDetailStrings(){
        Map<Integer, String> dictionary = new TreeMap<Integer, String>();
        DecimalFormat format = new DecimalFormat("#.##");

        if(vectorSets != null){
            for(Set<Node> layerset : vectorSets){
                for(Node node: layerset){
                    dictionary.put(node.getNodeNumber(), node.toString());
                }
            }
        }

        return dictionary.values();
    }

    public Vector<Set<Node>> getVectorSets(){
        return this.vectorSets;
    }

    /**
     * Set the default learning rate
     * @param learningRate
     */
    public void setDefaultLearningRate(double learningRate){
        this.defaultLearningRate = learningRate;
    }

    /**
     * Set the default bias
     * @param bias
     */
    public void setDefaultBias(double bias){
        this.defaultBias = bias;
    }

    /**
     * Add a bias
     * @param nodeNumber node number
     * @param bias bias
     * @return return success
     */
    public void addBias(int nodeNumber, double bias){
        if(!biasDictionary.keySet().contains(nodeNumber)){
            biasDictionary.put(nodeNumber, bias);
        } else {
            biasDictionary.remove(nodeNumber);
            biasDictionary.put(nodeNumber, bias);
        }

        if(nodeDictionary != null) {
            if (nodeDictionary.containsKey(nodeNumber)) {
                nodeDictionary.get(nodeNumber).setBias(bias);
            }
        }
    }

    /**
     * add a new learning rate
     * @param nodeNumber
     * @param learningRate
     * @return
     */
    public void addLearningRate(int nodeNumber, double learningRate){
        if(!learningRateDictionary.keySet().contains(nodeNumber)){
            learningRateDictionary.put(nodeNumber, learningRate);
        } else {
            learningRateDictionary.remove(nodeNumber);
            learningRateDictionary.put(nodeNumber, learningRate);
        }

        if(nodeDictionary != null) {
            if (nodeDictionary.containsKey(nodeNumber)) {
                nodeDictionary.get(nodeNumber).setLearningRate(learningRate);
            }
        }
    }

    public void changeWeight(int nodeNumber, int outputNodeNumber, double weight){
        if(nodeDictionary != null){
            if(nodeDictionary.containsKey(nodeNumber)){
                nodeDictionary.get(nodeNumber).setWeight(outputNodeNumber, weight);
            }
        }
    }

    public Node getNode(int nodeNumber){
        if(nodeDictionary != null){
            if(nodeDictionary.containsKey(nodeNumber)){
                return nodeDictionary.get(nodeNumber);
            }
        }

        return null;
    }

    public void setInputValues(String... inputValues){
        if(inputValues.length != vectorSets.get(0).size()){
            throw new IllegalArgumentException("Input size does not match number of inputs");
        }

        int count = 0;
        for(Node node : vectorSets.get(0)){
            if(node instanceof InputNode){
                ((InputNode) node).setStrInput(inputValues[count++]);
            }
        }
    }

    public void setOutputValues(String... outputValues){
        if(outputValues.length != vectorSets.get(layercount - 1).size()){
            throw new IllegalArgumentException("Output size does not match number of outputs");
        }

        int count = 0;
        for(Node node : vectorSets.get(layercount - 1)){
            if(node instanceof OutputNode){
                ((OutputNode) node).setStrOutput(outputValues[count++]);
            }
        }
    }

    /**
     * create nodes in the neural map
     */
    public void createNodes() {
        this.vectorSets = new Vector<>();
        this.nodeDictionary = new TreeMap<>();

        for (int i = 0; i < layercount; i++) {
            Set<Node> tmpSet = new TreeSet<Node>();
            for (int n = 0; n < layerNodes[i]; n++) {
                Node node;

                if (i == 0) {
                    node = new InputNode();
                } else if (i == layercount - 1) {
                    node = new OutputNode();
                } else {
                    node = new Node();
                }
                tmpSet.add(node);
                nodeDictionary.put(node.getNodeNumber(), node);
            }
            vectorSets.add(tmpSet);
        }


        for (int i = 0; i < layercount; i++) {
            Set<Node> currentNodes = this.vectorSets.get(i);
            Set<Node> inputNodes = null;
            Set<Node> outputNodes = null;

            if (i > 0) {
                inputNodes = this.vectorSets.get(i - 1);
            } else {
                inputNodes = null;
            }

            if (i != layercount - 1) {
                outputNodes = this.vectorSets.get(i + 1);
            } else {
                outputNodes = null;
            }

            for (Node node : currentNodes) {
                double bias = defaultBias;
                double learningRate = defaultLearningRate;

                if (this.biasDictionary.keySet().contains(node.getNodeNumber())) {
                    bias = biasDictionary.get(node.getNodeNumber());
                }

                if (this.learningRateDictionary.keySet().contains(node.getNodeNumber())) {
                    bias = learningRateDictionary.get(node.getNodeNumber());
                }

                node.setParameter(inputNodes, outputNodes, defaultLearningRate, defaultBias);
            }
        }
    }

    /**
     * Forward propagation
     * @param inputs
     * @param isTraining
     * @return
     */
    public double[] forward(double[] inputs, boolean isTraining){
        if(inputs.length != vectorSets.get(0).size()){
            throw new IllegalArgumentException("Bad number of inputs per layer in input layer");
        }

        List<double[]> calc = null;
        if(isTraining){
            calc = new ArrayList<double[]>();
            storage.push(calc);
        }
        calc.add(inputs);
        return forwardHelper(inputs, 1, calc);
    }

    /**
     * forward propagation helper
     * @param inputs
     * @param layerNumber
     * @param calc
     * @return
     */
    private double[] forwardHelper(double[] inputs, int layerNumber, List<double[]> calc){
        if(layerNumber == layercount){
            return inputs;
        }

        if(inputs.length != vectorSets.get(layerNumber - 1).size()){
            throw new IllegalArgumentException("Bad number of inputs per layer in layer" + layerNumber );
        }

        Set<Node> set = vectorSets.get(layerNumber);
        int count = 0;
        double[] outputs = new double[set.size()];
        for (Node node : set){
            outputs[count++] = node.calculateOutput(inputs);
        }

        calc.add(outputs);
        return forwardHelper(outputs, layerNumber + 1, calc);
    }



    /**
     * perform backpropagation
     * https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/
     * @param expectedList
     */
    public void backpropagation(List<double[]> expectedList){
        if(storage.size() != expectedList.size()){
            throw new IllegalArgumentException("Invalid number of expected list. Expected " + storage.size());
        }

        for(double[] expected : expectedList){
            List<double[]> results = storage.pop();

            double[] error = NeuralMap.calculateError(results.get(results.size() -1), expected);
            double[] newExpected = null;
            double[] newExpected2 = null;
            for(int i = vectorSets.size() - 1; i > 0; i--){
                Set<Node> outputs = vectorSets.get(i);
                double[] resultOutput = results.get(i);

                if(newExpected == null){
                    newExpected = new double[vectorSets.get(i).size()];
                } else {
                    newExpected2 = new double[vectorSets.get(i).size()];
                }

                int count = 0;

                for(Node outputNode : outputs){

                    int offset = 0;
                    if (outputNode instanceof OutputNode) {
                        for (Node inputNode : outputNode.getInputNodes()) {
                            double errorOnOutput = -1.0 * (expected[count] - resultOutput[count]);
                            double netInput = resultOutput[count] * (1 - resultOutput[count]);

                            newExpected[count] = errorOnOutput * netInput;

                            double outputFromWeight = results.get(i - 1)[offset];

                            errorOnOutput = errorOnOutput * netInput * outputFromWeight;
                            inputNode.outputCostsTotal.put(outputNode, errorOnOutput);
                            //System.out.println(inputNode.getWeight(outputNode) - (inputNode.getLearningRate() * errorOnOutput));
                            offset++;
                        }
                        count++;
                    }
                    else {
                        for (Node inputNode : outputNode.getInputNodes()) {

                            double errorOnOutput = 0;
                            int newExpectedCount  = 0;
                            for(Node output : outputNode.getOutputNodes()){
                                errorOnOutput += outputNode.getWeight(output) * newExpected[newExpectedCount++];
                            }

                            double netInput = resultOutput[count] * (1 - resultOutput[count]);
                            double outputFromWeight = results.get(i - 1)[offset];

                            newExpected2[count] = errorOnOutput * netInput;
                            errorOnOutput = errorOnOutput * netInput * outputFromWeight;
                            inputNode.outputCostsTotal.put(outputNode, errorOnOutput);
                            //System.out.println(inputNode.getWeight(outputNode) - (inputNode.getLearningRate() * errorOnOutput));
                            offset++;
                        }
                        count++;
                    }
                }

                if(newExpected2 != null){
                    newExpected = newExpected2;
                    newExpected2 = null;
                }
            }
        }

        for(Node inputNode : nodeDictionary.values()) {
            if(inputNode.getOutputCostsTotal().size() > 0){
                for(Node outputNode : inputNode.getOutputCostsTotal().keySet()){
                    double cost = inputNode.getOutputCostsTotal().get(outputNode) / expectedList.size();
                    //System.out.println(inputNode.getWeight(outputNode) - (inputNode.getLearningRate() * errorOnOutput));
                    inputNode.setWeight(outputNode, inputNode.getWeight(outputNode) - (cost * inputNode.getLearningRate()));
                }

                inputNode.getOutputCostsTotal().clear();
            }
        }

    }

    /**
     * File to write to
     * @param file
     */
    public void saveData(File file){
        if(vectorSets == null){
            return;
        }
        try{
            FileWriter writer = new FileWriter(file);
            StringBuilder sb = new StringBuilder();

            writer.append(vectorSets.size() + ",");
            for(Set<Node> layer : vectorSets){
                writer.append(layer.size() + ",");
            }
            writer.append(System.getProperty("line.separator"));
            for(Set<Node> layer : vectorSets){
                for(Node node : layer){
                    writer.append(node.getNodeNumber() + ",");
                    writer.append(node.getBias() + ",");
                    writer.append(node.getLearningRate() + ",");
                    if(node instanceof InputNode){
                        writer.append("I," +  ((InputNode)node).getStrInput() + ",");
                    } else if (node instanceof OutputNode){
                        writer.append("O," +  ((OutputNode)node).getStrOutput() + ",");
                    } else {
                        writer.append("H, ,");
                    }
                    for(Node outputNode : node.getOutputNodes()){
                        writer.append(outputNode.getNodeNumber() + ",");
                        writer.append(node.getWeight(outputNode) + ",");
                    }
                    writer.append(System.getProperty("line.separator"));
                }
            }

            writer.write(sb.toString());
            writer.close();

        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public static NeuralMap readData(File file){
        NeuralMap map = null;
        try{
            FileReader reader = new FileReader(file);
            BufferedReader breader = new BufferedReader(reader);

            String parameter = breader.readLine();

            if(parameter != null) {
                String[] split = parameter.split(",");
                map = new NeuralMap(split);
                map.createNodes();
            }

            String line;
            while((line = breader.readLine()) != null){
                String[] split = line.split(",");

                Node node = map.getNode(Integer.parseInt(split[0]));
                node.setBias(Double.parseDouble(split[1]));
                node.setLearningRate( Double.parseDouble(split[2]));

                if(node instanceof InputNode){
                    ((InputNode) node).setStrInput(split[4]);
                } else if (node instanceof OutputNode){
                    ((OutputNode) node).setStrOutput(split[4]);
                }

                int index = 5;
                while(index < split.length){
                    node.setWeight(Integer.parseInt(split[index]), Double.parseDouble(split[index + 1]));
                    index += 2;
                }
            }

        }catch(Exception e){
            e.printStackTrace();
        }

        return map;
    }

    /**
     * calculate sigmoid
     * @param x
     * @return
     */
    public static double sigmoid(double x){
        double y;
        if( x < -10 )
            y = 0;
        else if( x > 10 )
            y = 1;
        else
            y = 1 / (1 + Math.exp(-x));

        return y;
    }

    /**
     * calculate derivative of sigmoid
     * @param x
     * @return
     */
    public static double sigmoid_derive(double x){
        return x * (1 - x);
    }

    /**
     * Calculate the error
     * @param calculated
     * @param expected
     * @return
     */
    private static double[] calculateError(double[] calculated, double[] expected){
        if(calculated.length != expected.length){
            throw new IllegalArgumentException("Invalid length between calculated and expected arrays");
        }

        double[] error = new double[expected.length];
        for(int i = 0; i < expected.length; i++){
            error[i] = Math.pow(expected[i] - calculated[i], 2) / 2;
        }

        return error;
    }

}
