package com.NeuralNetwork;

import java.util.*;

/**
 * Created by rpai on 6/27/2018.
 */
public class Node extends Object implements Comparable{

    private static int nodeCount = 0;

    protected Map<Node, Double> outputWeights;
    protected Map<Node, Double> outputCostsTotal;

    protected Set<Node> inputNodes;
    private double bias = 0;
    private double learningRate = 0.5;
    private int nodeNumber = 0;

    public Node() {
        this.nodeNumber = nodeCount++;
        inputNodes = new HashSet<>();
        outputCostsTotal = new HashMap<>();
        outputWeights = new HashMap<>();
    }

    public void setParameter(Set<Node> inputNodes, Set<Node> outputNodes){
        this.inputNodes = inputNodes;

        if(outputNodes != null) {
            for (Node node : outputNodes) {
                outputWeights.put(node, 0.5);
            }
        }
    }

    public static void resetNodeNumber(){
        Node.nodeCount = 0;
    }

    public void setParameter(Set<Node> inputNodes, Set<Node> outputNodes, double learningRate){
        setParameter(inputNodes, outputNodes);
        this.setLearningRate(learningRate);
    }

    public void setParameter(Set<Node> inputNodes, Set<Node> outputNodes, double learningRate, double bias){
        setParameter(inputNodes, outputNodes, learningRate);
        this.setBias(bias);
    }

    public int getNodeNumber(){
        return nodeNumber;
    }

    public Set<Node> getInputNodes(){
        return inputNodes;
    }

    public Set<Node> getOutputNodes(){
        return outputWeights.keySet();
    }

    public double getWeight(Node node){
        return outputWeights.get(node);
    }

    public double getWeight(int nodeNumber){
        for(Node node : outputWeights.keySet()){
            if(node.getNodeNumber() == nodeNumber){
                return outputWeights.get(node);
            }
        }

        return -1;
    }

    public void setWeight(Node node, double weight){
        if(outputWeights.keySet().contains(node)){
            outputWeights.remove(node);
            outputWeights.put(node, weight);
        }
    }

    public void setWeight(int nodeNumber, double weight){
        Node outputNode = null;
        for(Node node : outputWeights.keySet()){
            if(node.getNodeNumber() == nodeNumber){
                outputNode = node;
                break;
            }
        }

        if(outputNode != null)
        {
            outputWeights.remove(outputNode);
            outputWeights.put(outputNode, weight);
        }
    }

    public double calculateOutput(double[] inputValues){
        double x = 0;
        int count = 0;
        for(Node node: inputNodes){
            double value1 = node.getWeight(this);
            double value2 = inputValues[count++];
            x += (value1 * value2);
        }

        return NeuralMap.sigmoid(x + bias);
    }

    public double getBias() {
        return bias;
    }

    public void setBias(double bias) {
        this.bias = bias;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public String toString(){
        StringBuilder str = new StringBuilder();
        for(Node node : outputWeights.keySet()){
            str.append(node.getNodeNumber() + "-" + outputWeights.get(node).toString() + " ");
        }

        return String.format("NodeNumber:%d %s ", this.nodeNumber, str.toString());
    }

    public Map<Node, Double> getOutputCostsTotal() {
        return outputCostsTotal;
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof Node){
            return this.nodeNumber - ((Node) o).getNodeNumber();
        }

        return 0;
    }
}
