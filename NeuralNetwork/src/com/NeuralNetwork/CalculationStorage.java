package com.NeuralNetwork;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class CalculationStorage {

    private Queue<List<double[]>> storage;

    public CalculationStorage() {
        storage = new LinkedList<>();
    }

    public int size(){
        return storage.size();
    }

    public List<double[]> pop(){
        return storage.remove();
    }

    public void push(List<double[]> list){
        storage.add(list);
    }
}
