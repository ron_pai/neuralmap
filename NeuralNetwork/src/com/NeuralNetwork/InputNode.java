package com.NeuralNetwork;

/**
 * Created by rpai on 6/28/2018.
 */
public class InputNode extends Node {

    private String strInput;

    public InputNode(){
        super();
        strInput = null;
    }

    /**
     * getter for strInput
     * @return
     */
    public String getStrInput() {
        return strInput;
    }

    /**
     * Setter for strInput
     * @param strInput
     */
    public void setStrInput(String strInput) {
        this.strInput = strInput;
    }

    public String toString(){
        return strInput + "\t\t " + super.toString();
    }
}
