package com.NeuralNetwork;

/**
 * Created by rpai on 6/28/2018.
 */
public class OutputNode extends Node{
    private String strOutput;

    public OutputNode(){
        super();
        strOutput = null;
    }

    /**
     * getter for strInput
     * @return
     */
    public String getStrOutput() {
        return strOutput;
    }

    /**
     * Setter for strInput
     * @param strOutput
     */
    public void setStrOutput(String strOutput) {
        this.strOutput = strOutput;
    }

    public String toString(){
        return strOutput + "\t\t " + super.toString();
    }
}
